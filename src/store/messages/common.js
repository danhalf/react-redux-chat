const ActionType = {
    SET_MESSAGES: 'messages/set-messages',
    ADD: 'messages/add',
    EDIT: 'messages/edit',
    DELETE: 'messages/delete',
};

export { ActionType };