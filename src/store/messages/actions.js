import * as messages from '../database.json';
import { ActionType } from './common';
import { getRandomId, getISODate } from "../../Helpers/helpers";
import { MessagesKey } from "../../common/enums/enums"

const fetchMessages = () => ({
    type: ActionType.SET_MESSAGES,
    payload: {
        messages,
    },
});

const addMessage = (message) => ({
    type: ActionType.ADD,
    payload: {
        message: {
            [MessagesKey.ID]: getRandomId(),
            [MessagesKey.TEXT]: message,
            [MessagesKey.CREATED]: getISODate(),
            [MessagesKey.LIKED]: [],
            [MessagesKey.EDITED]: "",
            [MessagesKey.USER_ID]: "uid",
            [MessagesKey.USER_NAME]: "uname",
            [MessagesKey.AVATAR]: "u_avatar"
        }
    },
});

const deleteMessage = (messageId) => ({
    type: ActionType.DELETE,
    payload: {
        messageId
    },
});

const editMessage = ({id, text}) => ({
    type: ActionType.EDIT,
    payload: {
        id,
        text
    }
})


export { fetchMessages, addMessage, deleteMessage, editMessage }