import { ActionType } from './common';
import { getISODate } from '../../Helpers/helpers';

const initialState = {
    "chat": {
        "messages": [
        ],
        "editModal": false,
        "preloader": true,
    }
};

const reducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case ActionType.SET_MESSAGES: {
            const { messages } = payload.messages;
            return {
                ...state.chat,
                messages,
            };
        }

        case ActionType.ADD: {
            const { message } = payload;
            return {
                ...state.chat,
                messages: state.messages.concat(message),
            };
        }

        case ActionType.DELETE: {
            const { messageId } = payload;
            return {
                ...state.chat,
                messages: state.messages.filter((curMessages) => curMessages.id !== messageId),
            };
        }

        case ActionType.EDIT: {
            const {id, text} = payload
            return {
                ...state.chat,
                messages: state.messages.map(msg => {
                    return msg.id === id ? {...msg, text, editedAt: getISODate()} : msg
                })
            }
        }

        default: {
            return state;
        }
    }
};

export { reducer };