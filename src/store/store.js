import { combineReducers, createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import { messages } from './root-reducer';


const store = createStore(
    combineReducers({
        messages,
        // ownMessages
    })
)

export { store }