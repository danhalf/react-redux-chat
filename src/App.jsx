import React from 'react';
import './App.css';
// export { DataStatus } from './data-status.enum';
import Chat from './bsa';
import { Route, Routes } from "react-router-dom";
import { AppPath } from "./common/enums/app/app-path.enum";

function App() {
  // const CHAT_URL = process.env.REACT_APP_URL;
  return (
    <div className="app">
        <Routes>
            {/*<Route path={AppPath.LOGIN} element={<Login />} />*/}
            <Route path={AppPath.ROOT} element={<Chat />} />
        </Routes>
    </div>
  );
}

export default App;
