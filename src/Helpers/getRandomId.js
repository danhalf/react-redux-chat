import { getISODate } from './helpers';

const date = getISODate()

export const getRandomId = () => `${date.toString()}-${Math.random()}`