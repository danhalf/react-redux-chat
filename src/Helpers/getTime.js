const getISODate = () => new Date().toISOString()

const getTime = fullDate => {
    const date = new Date(fullDate);
    return `${ date.getHours().toString().padStart(2, 0) }:${ date.getMinutes().toString().padStart(2, 0) }`
}

const getLastMessageTime = (messagesState) => {
    const [ lastMessageData ] = Object.values(messagesState).map(res=> {
        //TODO ref this func
        const date = new Date(res.createdAt)
        return date.getTime()
    }).sort((a, b)=> b - a);
    const lastDate = new Date(lastMessageData)
    return `${lastDate.getDate()}.${lastDate.getMonth().toString().padStart(2, 0)}.${lastDate.getFullYear()} ${lastDate.getHours().toString().padStart(2, 0)}:${lastDate.getMinutes().toString().padStart(2, 0)}:${lastDate.getSeconds().toString().padStart(2, 0)}`
}

export {getISODate, getTime, getLastMessageTime};