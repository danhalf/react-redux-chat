import PropTypes from 'prop-types';
import { MessagesKey } from "../enums/enums";

const messageType = PropTypes.exact({
    [MessagesKey.ID]: PropTypes.string.isRequired,
    [MessagesKey.USER_ID]: PropTypes.string.isRequired,
    [MessagesKey.TEXT]: PropTypes.string.isRequired,
    [MessagesKey.AVATAR]: PropTypes.string.isRequired,
    [MessagesKey.USER_NAME]: PropTypes.string.isRequired,
    [MessagesKey.CREATED]: PropTypes.string.isRequired,
    [MessagesKey.EDITED]: PropTypes.string.isRequired,
});

export { messageType };
