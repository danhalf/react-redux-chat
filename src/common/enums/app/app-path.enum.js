const AppPath = {
    ROOT: '/',
    LOGIN: '/login',
    REGISTRATION: '/registration',
    MESSAGES: '/messages',
    MESSAGES_$ID: '/messages/:id',
    ANY: '*',
};

export { AppPath };