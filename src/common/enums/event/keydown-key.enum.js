const KeydownKey = {
  ESCAPE: 'Escape',
  TAB: 'Tab',
  UP: 'Up'
};

export { KeydownKey };