import { useState, useEffect, useCallback } from 'react';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import Loader from "../Loader/Loader";
import { messages as messagesActionCreator } from '../../store/actions'
import MessageInput from "../MessageInput/MessageInput";
import { useDispatch, useSelector } from "react-redux";

const Chat = () => {
    const userId = "uid"
    const { messages } = useSelector(({ messages }) => ({
        messages: messages.messages
    }));
    const [ messagesCount, setMessagesCount ] = useState(0)
    const [ uniqUsersCount, setUniqUsersCount ] = useState(0)
    const [ isLoaded, setIsLoaded ] = useState(false);

    const dispatch = useDispatch();

    const handleMessageAdd = useCallback((message) => {
        dispatch(messagesActionCreator.addMessage(message));
    }, [dispatch]);

    const handleMessageDelete = useCallback((message) => {
        dispatch(messagesActionCreator.deleteMessage(message));
    }, [dispatch]);


    useEffect(() => {
        dispatch(messagesActionCreator.fetchMessages());
        setIsLoaded(true)
    }, [ dispatch ]);


    useEffect(() => {
        setMessagesCount(messages?.length)
        setUniqUsersCount([ ...new Set(messages?.map(({ userId }) => userId)) ].length)
    }, [ messages ]);


    return (
        <>
            <Header uniqUsersCount={ uniqUsersCount } messagesCount={ messagesCount }/>
            { isLoaded ? <MessageList messages={ messages } onMessageDelete={handleMessageDelete} currentUserId={userId}/> : <Loader/> }
            <MessageInput onMessageAdd={handleMessageAdd} />
        </>
    );
}

export default Chat
