const Loader = () => {
    let circleCommonClasses = 'h-6 w-6 bg-lime-400   rounded-full';

    return (
        <div className='flex justify-center preloader'>
            <div className={ `${ circleCommonClasses } mr-1 animate-bounce` }/>
            <div className={ `${ circleCommonClasses } mr-1 animate-bounce200` }/>
            <div className={ `${ circleCommonClasses } animate-bounce400` }/>
        </div>
    );
};

export default Loader;