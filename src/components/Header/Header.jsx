const Header = (props) => {

  return (
    <header className="header bg-green-600 flex justify-between items-center h-24 min-h-full">
      <h2 className="header-title">Chat Title</h2>
      <div className="header-users-count">Users count: {props.uniqUsersCount}</div>
      <div className="header-messages-count">Messages count: {props.messagesCount}</div>
      <div className="header-last-message-date">Last message time: {props.lastMessageTime}</div>
    </header>
  );
}

export default Header;
