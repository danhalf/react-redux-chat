import Message from './Message/Message';
import OwnMessage from './OwnMessage/OwnMessage';
import { useCallback, useState } from 'react';
import EditMessageModal from '../EditMessageModal/EditMessageModal';
import { useDispatch } from 'react-redux';
import { messages as messagesActionCreator } from '../../store/actions';

function MessageList({ messages, currentUserId, onMessageDelete }) {

  const [ currentMessage, setCurrentMessage ] = useState(null);

  const dispatch = useDispatch();

  const hasCurrentMessage = Boolean(currentMessage);

  const handleEditMessageModalClose = () => setCurrentMessage(null);

  const handleMessageSave = useCallback((message) => {
    dispatch(messagesActionCreator.editMessage(message));
    setCurrentMessage(null);
  }, [ dispatch ]);

  const handleMessageEdit = (message) => setCurrentMessage(message);

  console.log(messages);

  return <section className='messages'>
    <ul className='message-list flex flex-col'>
      { Object.values(messages).map(({ userId, id, text, createdAt, editedAt, avatar, user }) => (
        userId === currentUserId
          ? <OwnMessage key={ id }
                        id={ id }
                        text={ text }
                        time={ createdAt }
                        editedAt={ editedAt }
                        onMessageDelete={ onMessageDelete }
                        onMessageEdit={ handleMessageEdit } />
          : <Message key={ id } id={ id } avatar={ avatar } user={ user }
                     text={ text } time={ createdAt } editedAt={ editedAt } />
      )) }

    </ul>
    { hasCurrentMessage && <EditMessageModal message={ currentMessage } onSave={ handleMessageSave }
                                             onClose={ handleEditMessageModalClose } /> }
  </section>;
}

export default MessageList;
