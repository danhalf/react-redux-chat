import { getTime } from "../../../Helpers/getTime";
import { useState } from "react";

const Message = (props) => {

    const [ isLiked, setIsLiked ] = useState(false);

    const onLikedMessage = () => {
        setIsLiked(!isLiked)
    }

    return <li className='message flex m-4 rounded-md w-2/3 justify-between items-center bg-lime-100 p-2 '>
        <div className="flex flex-col justify-center items-center border-r-2 p-4">
            <img className="w-10 h-10 rounded-full " src={ props.avatar } alt='user-avatar'/>
            <span className="message-user-name">
                { props.user }
            </span>
        </div>
        <div className="text-right pl-4">
            <span className="message-text">{ props.text }</span>
            <div className="message-time text-right opacity-10 mt-4">{ getTime(props.time) }</div>
            <button onClick={ onLikedMessage } className="inline message-like w-10 h-10 text-2xl rounded">{
                isLiked
                ? <span className="text-red-400 hover:text-red-600">&hearts;</span>
                : <span className="text-black hover:text-red-600">&#9829;</span> }</button>
        </div>
    </li>;
}

export default Message