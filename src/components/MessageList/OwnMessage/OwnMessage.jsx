import { getTime } from "../../../Helpers/getTime";
const OwnMessage = ({id, time, editedAt, text, onMessageDelete, onMessageEdit}) => {

    const handleMessageDelete = () => {
        onMessageDelete(id)
    }

    const handleMessageEdit = () => {
        onMessageEdit({ id, text })
    }

    return <li className='own-message flex m-4 rounded-md w-2/3 justify-between items-center bg-lime-100 p-2 ml-auto'>
        <div className="text-left pl-4">
            <span className="message-text">{ text }</span>
            <div className="message-time text-left opacity-10 mt-4">{ editedAt ? `edited at: ${getTime(editedAt)}` :`sent at: ${getTime(time)}`}</div>
            <button onClick={handleMessageDelete}>
                <svg className="h-5 w-5 text-red-300 hover:text-red-500" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                     strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                    <polyline points="3 6 5 6 21 6"/>
                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"/>
                    <line x1="10" y1="11" x2="10" y2="17"/>
                    <line x1="14" y1="11" x2="14" y2="17"/>
                </svg>
            </button>
            <button onClick={handleMessageEdit}>
                <svg className="h-5 w-5 text-blue-300 hover:text-blue-500 ml-2" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor"
                     fill="none" strokeLinecap="round" strokeLinejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"/>
                    <path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"/>
                    <path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"/>
                    <line x1="16" y1="5" x2="19" y2="8"/>
                </svg>
            </button>
        </div>

    </li>;
}

export default OwnMessage