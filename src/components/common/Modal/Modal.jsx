import { useCallback, useEffect } from 'react';
import { KeydownKey } from '../../../common/enums/enums';

const Modal = ({ children, isOpen, onClose }) => {
  const handleEscapeDown = useCallback(({ key }) => {
    if (key === KeydownKey.ESCAPE) {
      onClose();
    }
  }, [ onClose ]);

  const handleOverlayCheck = ({ target }) => {
    if (!target.closest('section')) {
      onClose();
    }
  };

  useEffect(() => {
    document.documentElement.style.overflowY = 'hidden';
    document.addEventListener('keydown', handleEscapeDown);

    return () => {
      document.documentElement.style.overflowY = '';
      document.removeEventListener('keydown', handleEscapeDown);
    };
  }, [ handleEscapeDown ]);

  return (
    <>
      { isOpen && (
        <div className={`${isOpen ? '.edit-message-modal .modal-shown': '.edit-message-modal'}` } onClick={ handleOverlayCheck }>
          { children }
        </div>
      ) }
    </>
  );
};




export default Modal;
